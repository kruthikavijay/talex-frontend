var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function($routeProvider, $locationProvider) {
  //  $locationProvider.hashPrefix('');
  $routeProvider

  .when('/', {
    templateUrl: 'components/login.html',
    controller: 'loginController'
  })

  .when('/dashboard', {
    templateUrl: 'components/dashboard.html',
    controller: 'dashboardController'
  })
  .when('/createJob',{
    templateUrl:'components/createJob.html',
    controller:'createJobController'
  })
  .when('/manageJobs',{
    templateUrl:'components/manageJobs.html',
    controller:'manageJobController'
  })
  .when('/searchResult',{
    templateUrl:'components/searchResult.html',
    controller:'candidateSearchController'
  })
  .when('/jobInvite',{
    templateUrl:'components/job-invite.html',
    controller:'jobInviteController'
  })
  
});
myApp.service('headerService',function(){
  
  });
// myApp.controller('loginController',['$scope',' $location','headerService', function($scope,$location, headerService){
//   $scope.headerService=headerService;
//   alert(headerService);
//   $scope.headerService.show=false;
//   $scope.login=function(){
//     $location.path('/dashboard');
  
//   }
// }]);
myApp.controller('loginController',['$scope','headerService', '$location', function($scope, headerService, $location){
  
 $scope.headerService=headerService;
 
 $scope.headerService.show=false;
 $scope.login=function(){
    $location.path('/dashboard');
    
     }
}]);
myApp.controller('dashboardController',['$scope','headerService', function($scope,headerService){
    
   $scope.headerService=headerService;
   
   $scope.headerService.show=true;
  }]);
  myApp.controller('headerController',['$scope','headerService', function($scope,headerService){
    
   $scope.headerService=headerService;
  }]);

myApp.controller('createJobController',['$scope','headerService', '$location', function($scope, headerService, $location){
  $scope.headerService=headerService;
  $scope.headerService.show=true;
  $scope.search = function(){
    $location.path('/searchResult');
  }
  
}]);

myApp.controller('manageJobController', ['$scope','headerService', function($scope, headerService){
  $scope.headerService=headerService;
  $scope.headerService.show=true;
}]);

myApp.controller('candidateSearchController',['$scope','headerService', function($scope, headerService){
  $scope.headerService=headerService;
  $scope.headerService.show=true;
}]);

myApp.controller('jobInviteController',['$scope','headerService', '$location', function($scope, headerService, $location){
  $scope.headerService=headerService;
  $scope.headerService.show=true;
  $scope.submitInvite = function(){
    $location.path('/searchResult');
  }
}]);


